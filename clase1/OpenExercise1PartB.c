// Diego Portero y Marco Rodriguez
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define NUM_THREADS 8
int main(){
	omp_set_num_threads(NUM_THREADS);
	#pragma omp parallel
	{
		int ID =omp_get_thread_num();
		printf(" hello(%d)",ID);
		printf(" world(%d)\n",ID);
	}
}
