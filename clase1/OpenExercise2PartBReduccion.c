// Diego Portero y Marco Rodriguez
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#define NUM_THREADS 4
static long num_steps = 1000000;
double step;

int main(){
	int i;
	double x, pi, sum = 0.0;
	double start_time,run_time;
	step = 1.0/(double)num_steps;
	start_time =  omp_get_wtime();
	omp_set_num_threads(NUM_THREADS);
	#pragma omp parallel
	{
		double x;
		#pragma omp for reduction(+:sum)
			for(i=0;i<num_steps;i++){
				x=(i+0.5)*step;
				sum=sum+4.0/(1.0+x*x);
			}
		}
	pi = step*sum;
	run_time = omp_get_wtime()-start_time;
	printf("pi with %ld steps is %lf in %lf senconds\n",num_steps,pi,run_time);
}
