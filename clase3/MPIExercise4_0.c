//Completar el ejercicio donde falta inicialización, envío de intervalos, recogida de resultados y finalización.
//Usar envíos y recepciones bloqueantes

#include <stdio.h>
#include <math.h>
#include <mpi.h>


int main(int argc, char *argv[]) {
    int done = 0, n, myid, numprocs, i;
    double mypi, pi, h, sum, x;
    double startwtime, endwtime;
    double PI25DT = 3.141592653589793238462643;

    // 1.Inicializar  MPI
    int rank,count;
    int ntotalproc;
    int msg;
    MPI_Status status;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&ntotalproc);
  // //  ...
    n = 0;
    while (!done)
    {
        if (myid == 0)
        {

            printf("Indique número de intervalos (0 para salir)\n", n);
            scanf("%d",&n);

       // 2. Enviar .n. a todos
             int i;
       for (i = 0; i < ntotalproc; i++) {
           MPI_Send(&n,1,MPI_INT,i,100,MPI_COMM_WORLD);
            startwtime = MPI_Wtime();
        }
      }
      if (n == 0){
           done = 1;
      }else
        {
            MPI_Recv(&n,1,MPI_INT,0,99,MPI_COMM_WORLD,&status);
            printf("Hola hasta aqui\n");
            h   = 1.0 / (double) n;
            sum = 0.0;
            for (i = myid + 1; i <= n; i += numprocs)
            {
                x = h * ((double)i - 0.5);
                sum += 4.0 / (1.0 + x*x);
            }
            mypi = h * sum;
            printf("%f\n",mypi);
            MPI_Send(&mypi,1,MPI_DOUBLE,0,100,MPI_COMM_WORLD);
      }


		// 3. Enviar resultados parciales

//		...
            if (myid == 0)
	           {
              for (size_t i = 0; i < count; i++) {
                mypi=0;
                MPI_Recv(&mypi,1,MPI_DOUBLE,i,99,MPI_COMM_WORLD,&status);
                pi=pi+mypi;
              }
                printf("pi es aproximadamente %.16f, error relativo %16.8e\n", pi, (double)100 * (pi - PI25DT)/PI25DT);
	              endwtime = MPI_Wtime();
	              printf("tiempo empleado = %f\n", endwtime-startwtime);
	             }
        }


      MPI_Finalize();
    // 4. Finalizar  MPI

  //  ...


    return 0;
}
