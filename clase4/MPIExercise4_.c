#include <stdio.h>
#include <math.h>
#include <mpi.h>

int main(int argc, char **argv) {
    int done = 0, n=1, myid, numprocs, i;
    double mypi, pi, h, sum, x;
    double startwtime, endwtime;
    double PI25DT = 3.141592653589793238462643;
  	MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    while (!done)
    {
        if (myid == 0)
        {
            printf("Indique número de intervalos (0 para salir)\n");
            scanf("%d",&n);

            startwtime = MPI_Wtime();
        }
    MPI_Bcast(&n,1,MPI_INT,0,MPI_COMM_WORLD);
		if (n == 0){
			done = 1;
		}else{
			if(myid != 0){
				h   = 1.0 / (double) n;
	            sum = 0.0;
	            for (i = myid + 1; i <= n; i += (numprocs-1))
	            {
	                x = h * ((double)i + 0.5);
	                sum += 4.0 / (1.0 + x*x);
	            }
	            mypi = h * sum;
				printf("Soy el hijo %d y mypi = %f y mi n es = %d \n", myid, mypi,n);
				MPI_Send(&mypi, 1, MPI_DOUBLE, 0,200,MPI_COMM_WORLD);
    	}else{
				for(i=1; i<numprocs;i++){
					MPI_Recv(&sum, 1, MPI_DOUBLE, i,200,MPI_COMM_WORLD, &status);
					pi+=sum;
				}
				printf("pi es aproximadamente %.16f, error relativo %16.8e\n", pi, (double)100 * (pi - PI25DT)/PI25DT);
				endwtime = MPI_Wtime();
				printf("tiempo empleado = %f\n", endwtime-startwtime);
			}
		}
	}
    MPI_Finalize();
    return 0;
}
