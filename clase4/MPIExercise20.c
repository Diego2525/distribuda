
#include <stdio.h>
#include <string.h>
#include <mpi.h>
int main(int argc, char  **argv) {
  int rank,count;
  int ntotalproc;
  //MPI_Comm_size(MPI_COMM_WORLD);
  //char msg[20];
  int a=1;
  MPI_Status status;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  if(rank==0){
    printf("I am master. Sending the message.\n\n");
    // strcpy(msg,"Hello World!");
  }
    MPI_Bcast(&a,1,MPI_INT,0,MPI_COMM_WORLD);
    if(rank!=0){
      printf("I am the slave %d. Receiving the message.\n", rank);
      printf("The message is: %i\n",a);
    }

  MPI_Finalize();
  return 0;
}
