from socket import *
from time import ctime

HOST = 'localhost'
PORT = 5000
BUFSIZE = 1024
ADDRESS = (HOST,PORT)

server = socket(AF_INET, SOCK_STREAM)
server.bind(ADDRESS)
server.listen(5)

client, address = server.accept()
# client.send('Welcome to my chat room!') #Send greeting
while True:
	message = client.recv(BUFSIZE)   #Reply from client
	if message == "0": break
	if not message:
		print 'Client disconnected'
		client.close()
		break
	else:
		print message
		client.send(raw_input('> ')) #Reply to client
		if message == "0": break
client.close()
