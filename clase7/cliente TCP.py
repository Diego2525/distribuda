import socket
TCP_IP = 'localhost'
TCP_PORT = 5007
BUFFER_SIZE = 1024
s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
s.connect((TCP_IP,TCP_PORT))
conn, addr = s.accept()

while True:
    MESSAGE = raw_input()
    s.send(MESSAGE)
    data = conn.recv(BUFFER_SIZE)
    if not data: break
    print data
    conn.send(data)
    if MESSAGE == "0": break
s.close()
