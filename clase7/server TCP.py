import socket
from datetime import date
import calendar

TCP_IP = 'localhost'
TCP_PORT = 5007
BUFFER_SIZE = 20  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn
# print 'Connection address:', addr

while 1:
    MESSAGE = raw_input()
    s.send(MESSAGE)
    data = conn.recv(BUFFER_SIZE)
    if not data: break
    print data
    conn.send(data)
    if data == "0": break
conn.close()
