import socket
from datetime import date
import calendar

UDP_IP = "192.168.43.173"
UDP_PORT = 5001

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

while True:
     data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
     fecha = data.split("-")
     my_date = date(int(fecha[0]),int(fecha[1]),int(fecha[2]))
     data = calendar.day_name[my_date.weekday()]
     print "received message:", data
