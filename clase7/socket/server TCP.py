import socket
from datetime import date
import calendar

TCP_IP = '192.168.43.174'
TCP_PORT = 5007
BUFFER_SIZE = 20  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
print 'Connection address:', addr
while 1:
     data = conn.recv(BUFFER_SIZE)
     if not data: break
     print "received data:", data
     fecha = data.split("-")
     my_date = date(int(fecha[0]),int(fecha[1]),int(fecha[2]))
     data = calendar.day_name[my_date.weekday()]
     conn.send(data)  # echo
conn.close()
