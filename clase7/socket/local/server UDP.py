import socket
from datetime import date
import calendar

UDP_IP = '127.0.0.1'
UDP_PORT = 5001

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

while True:
     data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
     anio,mes,dia = data.split("-")
     my_date = date(int(anio),int(mes),int(dia))
     data = calendar.day_name[my_date.weekday()]
     print "received message:", data
