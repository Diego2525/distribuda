//install sudo apt install libopenmpi-dev
#include <stdio.h>
#include "mpi.h"
int main(int argc, char const *argv[]) {
  int nproc,myrank;
  MPI_Init(NULL,NULL);
  MPI_Comm_size(MPI_COMM_WORLD,&nproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
  printf("Hola, soy el proc. %d de %d\n",myrank,nproc);
  MPI_Finalize();
  return 0;
}
