
#include <stdio.h>
#include <string.h>
#include <mpi.h>
int main(int argc, char  **argv) {
  int rank,count;
  int ntotalproc;
  //MPI_Comm_size(MPI_COMM_WORLD);
  char msg[20];
  MPI_Status status;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  if(rank==0){
    printf("I am master. Sending the message.\n\n");
    strcpy(msg,"Hello World!");
    MPI_Send(msg,13,MPI_CHAR,1,100,MPI_COMM_WORLD);
  }else{
    printf("I am the slave %d. Receiving the message.\n", rank);
    MPI_Recv(msg,13,MPI_CHAR,0,100,MPI_COMM_WORLD,&status);
    printf("The message is: %s\n",msg);
    MPI_Get_count(&status,MPI_CHAR,&count);
    printf("Recibimos %d caracteres de %d con tag=%d\n",count,status.MPI_SOURCE,status.MPI_TAG);
  }
  MPI_Finalize();
  return 0;
}
