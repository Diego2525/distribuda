from socket import *
from time import ctime
from threading import Thread

class ClientHanler(Thread):
    def __init__(self, client):
        Thread.__init__(client)
        self._client = client

    def run(self):
        self._client.send(ctime()+'\nHave a nice day!')
        self._client.close()

HOST = 'localhost'
PORT = 5000
ADDRESS = (HOST,PORT)

server = socket(AF_INET,SOCK_STREAM)
server.bind(ADDRESS)
server.listen(5)

while True:
    print 'Waiting for connection ...'
    client, address = server.accept()
    handler = ClientHandler(client)
    handler.start()
