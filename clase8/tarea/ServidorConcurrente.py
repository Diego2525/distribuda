from socket import *
from time import ctime
from threading import Thread
#if __name__ == '__main__':
#    pass


class ClientHandler(Thread):
	"Handles a client Request"
	def __init__(self,client):
		Thread.__init__(self)
		self._client = client
		self._record = record

	def run(self):
		name = self._client.recv(BUFSIZE)   #Reply from client
		while True:
			message = self._client.recv(BUFSIZE)   #Reply from client
			print name,
			print message
			# broadcast(message,_client);
			if not message:
				print 'Client disconnected'
				client.close()
				break
		# self._client.send(ctime()+'\n Have a nice day! ')
		# self._client.close()

HOST = 'localhost'
PORT = 5002
BUFSIZE = 1024
ADDRESS = (HOST,PORT)

server = socket(AF_INET, SOCK_STREAM)
server.bind(ADDRESS)
server.listen(5)

#The server now just waits for connections from clients
#and hands sockets off to client handlers
while True:
	print 'waiting for connection...'
	client, address = server.accept()
	print '...connected from:', address
	handler = ClientHandler(client)
	handler.start()
