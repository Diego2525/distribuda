'''
Created on 22 nov. 2018
Python 3.2
Simple Chat Client Script
@author: leo
'''

from socket import *

HOST = 'localhost'
PORT = 5000
BUFSIZE = 1024

ADDRESS = (HOST, PORT)

server = socket(AF_INET, SOCK_STREAM)
server.connect(ADDRESS)
dayAndTime = server.recv(BUFSIZE) #The servers greeting
while True:
	message = raw_input('>') # Get my reply or quit
	if not message:
		break
	server.send(message + '\n') # Send my reply to the server
	reply = server.recv(BUFSIZE) #Get the server's reply
	if not reply:
		print 'Server disconnected'
		break
	print reply  #display the server's reply
server.close()
