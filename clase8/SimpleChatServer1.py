'''
Created on 22 nov. 2018
Python 3.2
Simple Chat Server Script
@author: leo
'''
from socket import *
from time import ctime
#if __name__ == '__main__':
#    pass

HOST = 'localhost'
PORT = 5000
BUFSIZE = 1024
ADDRESS = (HOST,PORT)

server = socket(AF_INET, SOCK_STREAM)
server.bind(ADDRESS)
server.listen(5)

while True: 
	print 'Waiting for connection...'
	client, address = server.accept()
	print '...connected from:', address
	client.send('Welcome to my chat room!') #Send greeting
	
	while True: 
		message = client.recv(BUFSIZE)   #Reply from client
		if not message: 
			print 'Client disconnected'
			client.close()
			break
		else:
			print message
			client.send(raw_input('> ')) #Reply to client
	


