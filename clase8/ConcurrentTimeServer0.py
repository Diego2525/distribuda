'''
Created on 22 nov. 2018
Python 3.2
Concurrent Time Server
@author: leo
'''
from socket import *
from time import ctime
from threading import Thread
#if __name__ == '__main__':
#    pass
class ClientHandler(Thread):
	"Handles a client Request"
	def __init__(self,client):
		Thread.__init__(self)
		self._client = client

	def run(self):
		self._client.send(ctime()+'\n Have a nice day! ')
		self._client.close()

HOST = 'localhost'
PORT = 5000
BUFSIZE = 1024
ADDRESS = (HOST,PORT)

server = socket(AF_INET, SOCK_STREAM)
server.bind(ADDRESS)
server.listen(5)

#The server now just waits for connections from clients
#and hands sockets off to client handlers

while True:
	print 'waiting for connection...'
	client, address = server.accept()
	print '...connected from:', address
	handler = ClientHandler(client)
	handler.start()
