'''
Created on 22 nov. 2018
Python 3.2
Day/Time Server Script
@author: leo
'''
from socket import *
from time import ctime
#if __name__ == '__main__':
#    pass

HOST = 'localhost'
PORT = 5000
ADDRESS = (HOST,PORT)

server = socket(AF_INET, SOCK_STREAM)
server.bind(ADDRESS)
server.listen(5)

while True: 
	print 'Waiting for connection...'
	client, address = server.accept()
	print '...connected from:', address
	client.send(ctime() + '\n Have a nice day!')
	client.close()


